﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
//using Finisar.SQLite;
using System.Data.SqlServerCe;
using System.Data.SQLite;
using System.Text.RegularExpressions;
using System.Threading;

namespace BackupPC
{
    public partial class DBQuery
    {
        #region Queries
        private static string m_createTargetFolder =
    @"CREATE TABLE TARGETFOLDER(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                Path nvarchar(256) NOT NULL);";
        private static string m_createBackup =
            @"CREATE TABLE mBACKUP(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                Name nvarchar(256) NOT NULL,
                mDesc nvarchar(256),
                Compress nvarchar(256) NOT NULL,
                TargetFoldID bigint NOT NULL,
                FOREIGN KEY(TargetFoldID) REFERENCES TARGETFOLDER(ID));";
        private static string m_createSourceFolder =
            @"CREATE TABLE SOURCEFOLDER(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                Path nvarchar(256) NOT NULL,
                BackupID bigint NOT NULL,
                FOREIGN KEY(BackupID) REFERENCES mBACKUP(ID));";
        private static string m_createTask =
                @"CREATE TABLE TASK(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                BackupType bigint NOT NULL,
                BackupStts bigint NOT NULL,
                Increment bigint NOT NULL,
                TTL bigint,
                BackupID bigint NOT NULL,
                FOREIGN KEY(BackupID) REFERENCES mBACKUP(ID));";
        private static string m_createTaskTime =
                @"CREATE TABLE TASKTIME(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                NextDate bigint NOT NULL,
                DayOfWeek bigint NOT NULL,
                TaskID bigint NOT NULL,
                FOREIGN KEY(TaskID) REFERENCES TASK(ID));";
        private static string m_createFolder =
                 @"CREATE TABLE FOLDER(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                Path nvarchar(256) NOT NULL,
                SourceFoldID bigint NOT NULL,
                FOREIGN KEY(SourceFoldID) REFERENCES SOURCEFOLDER(ID));";
        private static string m_createFile =
                 @"CREATE TABLE mFILE(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                Name nvarchar(256) NOT NULL,
                ModDate bigint NOT NULL,
                FoldID bigint NOT NULL,
                FOREIGN KEY(FoldID) REFERENCES FOLDER(ID));";
        private static string m_createTmpFile =
                 @"CREATE TABLE TMPFILE(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                Name nvarchar(256) NOT NULL,
                ModDate bigint NOT NULL,
                FoldPath nvarchar(256) NOT NULL,
                FoldID bigint,
                FileID bigint,
                SourceFoldID bigint);";
        private static string m_createRemoveItems =
                 @"CREATE TABLE REMOVEITEMS(
                ID bigint PRIMARY KEY IDENTITY(1,1),
                Path nvarchar(256) NOT NULL,
                DeleteDate bigint NOT NULL);";

        #endregion
        public void CreateDB()
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createTargetFolder;
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createBackup;
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createSourceFolder;
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createTask;
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createTaskTime;
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createFolder;
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createFile;
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createTmpFile;
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = m_createRemoveItems;
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Create tables exception;");
            }
        }
    }
}
