﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SQLite;
//using Finisar.SQLite;
using System.Data.SqlServerCe;
using System.Text.RegularExpressions;
using System.Threading;

namespace BackupPC
{
    public partial class DBQuery
    {
        private static string m_DBName = "test.sdf";
        private static string m_pass = "12345678";
        private SqlCeConnection m_connection;
        private SqlCeTransaction m_transaction;

        public static string DBName { get { return m_DBName; } }

        public static void InitMulti()
        {
           
        }

        public DBQuery(bool _isCreateNew = false)
        {
            string strConnection = string.Format("Data Source=\"{0}\"; Password ='{1}'", m_DBName, m_pass);
            
            if (_isCreateNew)
            {
                SqlCeEngine en = new SqlCeEngine(strConnection);
                en.CreateDatabase();
            }
            //string strConnection = string.Format("Data Source=\"{0}\"; Password ='{1}'",m_DBName,m_pass) ;
            m_connection = new SqlCeConnection(strConnection);
            m_connection.Open();
            m_transaction = null;
        }

        public void BeginTransaction()
        {
            m_transaction = m_connection.BeginTransaction();
        }

        public void CommitTransaction(bool _isCloseConnection = false)
        {
            m_transaction.Commit();
            m_transaction = null;
            if (_isCloseConnection) { this.CloseConnection(); }
        }

        public void RollbackTransaction(bool _isCloseConnection = false)
        {
            if (m_transaction != null) { m_transaction.Rollback(); }
            m_transaction = null;
            if (_isCloseConnection) { this.CloseConnection(); }
        }

        public long SelectLastInsertedID(string _table)
        {
            /*SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SELECT @@IDENTITY";
            
            var obj = cmd.ExecuteScalar();*/
            var cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SELECT MAX(ID) FROM " + _table;
            var obj = cmd.ExecuteScalar();
            return (long)obj;
        }
        /*
        public long SelectLastInsertedID()
        {
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SELECT @@IDENTITY";
            
            long obj = (long)cmd.ExecuteScalar();
            return 0;
        }*/

        public void CloseConnection()
        {
            m_connection.Close();
        }


    } 
}
