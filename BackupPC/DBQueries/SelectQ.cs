﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
//using Finisar.SQLite;
using System.Data.SqlServerCe;
using System.Data.SQLite;
using System.Text.RegularExpressions;
using System.Threading;

namespace BackupPC
{
    public partial class DBQuery
    {
        private List<long> SelectSourceFoldersID(long _backupID)
        {
            List<long> result = new List<long>();
            SqlCeCommand cmd = null;
            SqlCeDataReader reader = null;
            try
            {
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"SELECT ID FROM SOURCEFOLDER WHERE BackupID = @ID";
                cmd.Parameters.AddWithValue("@ID", _backupID);
                reader = cmd.ExecuteReader();
                while (reader.Read()) { result.Add((long)reader["ID"]); }
                reader.Close();
            }
            catch { if (reader != null) { reader.Close(); } throw new Exception("Select Source folder id"); }
            return result;
        }

        private List<long> SelectGroupTaskID(long _backupID)
        {
            List<long> result = new List<long>();
            SqlCeCommand cmd = null;
            SqlCeDataReader reader = null;
            try
            {
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"SELECT ID FROM TASK WHERE BackupID = @ID";
                cmd.Parameters.AddWithValue("@ID", _backupID);
                reader = cmd.ExecuteReader();
                while (reader.Read()) { result.Add((long)reader["ID"]); }
                reader.Close();
            }
            catch { if (reader != null) { reader.Close(); } throw new Exception("Select group task exception;"); }
            return result;
        }

        private ScheduleTask[] LoadScheduleTask(ScheduleTaskGroup _parent)
        {
            List<ScheduleTask> result = new List<ScheduleTask>();
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM TASKTIME WHERE TaskID = @ID";
            cmd.Parameters.AddWithValue("@ID", _parent.ID);
            SqlCeDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ScheduleTask task = new ScheduleTask(_parent, (DayOfWeek)(long)reader["DayOfWeek"], new DateTime((long)reader["NextDate"]));
                task.ID = (long)reader["ID"];
                result.Add(task);
            }
            reader.Close();
            return result.ToArray();
        }

        private long SelectFolderID(string _name, long _sourceFoldID)
        {
            long result = -1;
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SElECT ID FROM FOLDER WHERE SourceFoldID = @ID AND Path LIKE(@Path);";
            cmd.Parameters.AddWithValue("@ID", _sourceFoldID);
            cmd.Parameters.AddWithValue("@path", _name);

            SqlCeDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                result = (long)reader["ID"];
                                
            }
            reader.Close();
            return result;
        }

        private long SelectFileDate(ref long _id, string _name, long _foldID)
        {
            long ticks = -1;
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SElECT ID, ModDate FROM mFILE WHERE FoldID = @ID AND Name LIKE @Name;";
            cmd.Parameters.AddWithValue("@ID", _foldID);
            cmd.Parameters.AddWithValue("@Name", _name);

            SqlCeDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                _id = (long)reader["ID"];
                ticks = (long)reader["ModDate"];
            }
            reader.Close();
            return ticks;
        }

        private ScheduleTaskGroup[] LoadScheduleTaskGroup(Backup _backup)
        {
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SElECT * FROM TASK WHERE BACKUPID = @ID;";
            cmd.Parameters.AddWithValue("@ID", _backup.ID);
            SqlCeDataReader reader = cmd.ExecuteReader();
            List<ScheduleTaskGroup> result = new List<ScheduleTaskGroup>();
            while (reader.Read())
            {
                long id = (long)reader["ID"];
                ScheduleTaskGroup group = new ScheduleTaskGroup(
                    id,
                    (int)(long)reader["Increment"],
                    (int)(long)reader["TTL"],
                    (BackupType)(long)reader["BackupType"],
                    (BackupStatus)(long)reader["BackupStts"]);
                ScheduleTask[] tasks = LoadScheduleTask(group);
                group.UpdateTasksAndMinHour(tasks);
                group.Backup = _backup;
                result.Add(group);
            }
            reader.Close();
            return result.ToArray();
        }

        public bool IsBackupActive(long _id)
        {
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"select count(*) as rst from task
                                    where BackupStts not in ( @Fin ) and backupid = @ID";
            cmd.Parameters.AddWithValue("@ID", _id);
            cmd.Parameters.AddWithValue("@Fin", (int)BackupStatus.Finished);

            SqlCeDataReader reader = cmd.ExecuteReader();
            bool result = false;
            while (reader.Read())
            {
                result = (int)reader["rst"] > 0;
            } reader.Close();
            return result;
        }

        public bool IsAnyActive()
        {

            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"select count(*) from task
                                    where BackupStts not in ( @Fin )";
            cmd.Parameters.AddWithValue("@Fin", (long)BackupStatus.Finished);

            //SqlCeDataReader reader = cmd.ExecuteReader();
            bool result = (int)cmd.ExecuteScalar() > 0;
            /*while (reader.Read())
            {
                long count = (long)reader["rst"];
                result = (long)reader["rst"] > 0;
            } reader.Close();*/
            return result;
        }

        private BackupDir[] LoadSourceDirs(Backup _backup)
        {
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SElECT * FROM SOURCEFOLDER WHERE BACKUPID = @ID;";
            cmd.Parameters.AddWithValue("@ID", _backup.ID);
            SqlCeDataReader reader = cmd.ExecuteReader();
            List<BackupDir> result = new List<BackupDir>();
            while (reader.Read())
            {
                BackupDir dir = new BackupDir((long)reader["ID"], (string)reader["Path"]);
                dir.Backup = _backup;
                result.Add(dir);
            } reader.Close();
            return result.ToArray();
        }

        private BackupDir LoadTargetDir(long _dirID, Backup _backup)
        {
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SElECT * FROM TARGETFOLDER WHERE ID = @ID;";
            cmd.Parameters.AddWithValue("@ID", _dirID);
            SqlCeDataReader reader = cmd.ExecuteReader();
            reader.Read();
            BackupDir result = new BackupDir((long)reader["ID"], (string)reader["Path"]);
            result.Backup = _backup;
            reader.Close();
            return result;
        }

        public List<Backup> Load()
        {
            SqlCeCommand cmd = m_connection.CreateCommand();
            cmd.CommandText = @"SElECT * FROM mBACKUP;";
            SqlCeDataReader reader = cmd.ExecuteReader();
            List<Backup> result = new List<Backup>();
            while (reader.Read())
            {
                Backup b = new Backup();
                b.ID = (long)reader["ID"];
                b.IsArchive = bool.Parse((string)reader["Compress"]);
                b.Name = (string)reader["Name"];
                b.Description = (string)reader["mDesc"];
                b.TargetDir = this.LoadTargetDir((long)reader["TargetFoldID"], b);
                b.SourceDirs = this.LoadSourceDirs(b);
                b.ScheduleGroups = this.LoadScheduleTaskGroup(b);
                result.Add(b);
            }
            reader.Close();
            return result;
        }
    }
}
