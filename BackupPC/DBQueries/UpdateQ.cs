﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
//using Finisar.SQLite;
using System.Data.SqlServerCe;
using System.Text.RegularExpressions;
using System.Threading;

namespace BackupPC
{
    public partial class DBQuery
    {
        public void UpdateBackupDescription(Backup _backup)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"UPDATE mBACKUP SET mDesc = @Desc WHERE ID = @ID";
                cmd.Parameters.AddWithValue("@Desc", _backup.Description);
                cmd.Parameters.AddWithValue("@ID", _backup.ID);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Update backup description");
            }
        }

        public void UpdateBackupArchive(Backup _backup)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"UPDATE mBACKUP SET Compress = @Compress WHERE ID = @ID";
                cmd.Parameters.AddWithValue("@Compress", _backup.IsArchive.ToString());
                cmd.Parameters.AddWithValue("@ID", _backup.ID);
                cmd.ExecuteNonQuery();
            }
            catch 
            {
                throw new Exception("Update backup archive exception");
            }
        }

        public void UpdateTargetFolder(Backup _backup)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"UPDATE TARGETFOLDER SET Path = @Path WHERE ID = @ID";
                cmd.Parameters.AddWithValue("@Path", _backup.TargetDir.Value);
                cmd.Parameters.AddWithValue("@ID", _backup.TargetDir.ID);
                cmd.ExecuteNonQuery();
            } catch 
            {
                throw new Exception("Update target folder exception");
            }
        }

        public void UpdateSourceFolders(Backup _backup)
        {
            List<long> foldersID = SelectSourceFoldersID(_backup.ID);
            long[] realFoldersID = _backup.SourceDirs.Select(x => x.ID).ToArray();
            foreach (long id in foldersID)
            {
                if (!realFoldersID.Contains(id)) { DeleteSourceFolder(id); }
            }
            foreach (BackupDir dir in _backup.SourceDirs)
            {
                if (dir.ID < 0) { InsertSourceDir(dir); }
            }
        }

        public void UpdateSchedule(Backup _backup)
        {
            List<long> scheduleID = SelectGroupTaskID(_backup.ID);
            long[] realScheduleID = _backup.ScheduleGroups.Select(x => x.ID).ToArray();
            foreach (long id in scheduleID)
            {
                if (!realScheduleID.Contains(id)) { DeleteGroupTask(id); }
            }
            foreach (ScheduleTaskGroup group in _backup.ScheduleGroups)
            {
                if (group.ID < 0) 
                { 
                    InsertGroupTask(group);
                    foreach (ScheduleTask task in group.Tasks) { InsertTaskTime(task); }
                }
            }
        }

        public void UpdateStatus(ScheduleTaskGroup _group)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"UPDATE TASK SET BackupStts = @Stts WHERE ID = @ID";
                cmd.Parameters.AddWithValue("@Stts", (int)_group.Status);
                cmd.Parameters.AddWithValue("@ID", _group.ID);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Update status exception");
            }
        }

        private void UpdateFolderTmp(long _id, string _path, long _sourceID)
        {
            var cmd = m_connection.CreateCommand();
            cmd.CommandText = @"UPDATE TMPFILE SET FoldID = @ID WHERE SourceFoldID = @SourceID AND FoldPath = @Path";
            cmd.Parameters.AddWithValue("@ID", _id);
            cmd.Parameters.AddWithValue("@SourceID", _sourceID);
            cmd.Parameters.AddWithValue("@Path", _path);
            cmd.ExecuteNonQuery();
        }

        public void UpdateAllFoldersTmp()
        {
            var cmdSelect = m_connection.CreateCommand();
            bool flag = true;
            cmdSelect.CommandText = @"SELECT DISTINCT FoldPath, SourceFoldID FROM TMPFILE WHERE FoldID = -1";
            while (flag)
            {
                flag = false;
                var reader = cmdSelect.ExecuteReader();
                if (reader.Read())
                {
                    string path = (string)reader["FoldPath"];
                    long sourceID = (long)reader["SourceFoldID"];
                    long foldID = InsertFolder(path, sourceID);
                    reader.Close();
                    this.UpdateFolderTmp(foldID, path, sourceID);
                    flag = true;
                } else
                reader.Close();
                
            }
            
        }

        public void UpdateFilesFromTmp()
        {
            var cmd = m_connection.CreateCommand();
            cmd.CommandText = @"delete from mfile where id in (select fileid from tmpfile where fileid > 0)";
            /*cmd.CommandText = @"REPLACE INTO mFILE(ID,Name,ModDate,FoldID) 
                                    SELECT FileID, Name,ModDate,FoldID FROM TMPFILE 
                                    WHERE FileID > 0                                        
                                  ";*/
            cmd.ExecuteNonQuery();
            /*cmd = m_connection.CreateCommand();
            cmd.CommandText = @"insert into mfile(name,moddate,foldid) select name,moddate, foldid from tmpfile where fileid >";
            cmd.ExecuteNonQuery();*/
        }

        public void UpdateTaskRunTime(ScheduleTask _task)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"UPDATE TASKTIME SET NextDate = @Date WHERE ID = @ID";
                cmd.Parameters.AddWithValue("@Date", _task.NextDate.Ticks);
                cmd.Parameters.AddWithValue("@ID", _task.ID);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Update next date exception");
            }
        }
    }
}
