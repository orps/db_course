﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Finisar.SQLite;
//using System.Data.SQLite;
using System.Data.SqlServerCe;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;

namespace BackupPC
{
    public partial class DBQuery
    {

        private void InsertTargetFolder(BackupDir _dir)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO TARGETFOLDER(Path) VALUES(@Path)";
                cmd.Parameters.AddWithValue("@Path", _dir.Value);
                cmd.ExecuteNonQuery();
                _dir.ID = SelectLastInsertedID("targetFolder");
            }
            catch { throw new Exception("Insert target dir exception; path = " + _dir.Value); }
        }

        private void InsertGroupTask(ScheduleTaskGroup _group)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO TASK(BackupType,BackupStts,Increment,TTL,BackupID) VALUES(@Type,@Status,@Increment,@TTL,@ID)";
                cmd.Parameters.AddWithValue("@Type", (int)_group.Type);
                cmd.Parameters.AddWithValue("@Status", (int)_group.Status);
                cmd.Parameters.AddWithValue("@Increment", _group.RepeatAfterWeek);
                cmd.Parameters.AddWithValue("@TTL", _group.DeleteCopyAfter);
                cmd.Parameters.AddWithValue("@ID", _group.Backup.ID);
                cmd.ExecuteNonQuery();
                _group.ID = SelectLastInsertedID("task");
            }
            catch { throw new Exception("Insert grouptask exception; type = " + _group.Type.ToString()); }
        }

        private void InsertSourceDir(BackupDir _dir)
        {
      
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO SOURCEFOLDER(Path,BackupID) VALUES(@Path, @ID)";
                cmd.Parameters.AddWithValue("@Path", _dir.Value);
                cmd.Parameters.AddWithValue("@ID", _dir.Backup.ID);
                cmd.ExecuteNonQuery();
                _dir.ID = SelectLastInsertedID("sourcefolder");
            }
            catch { throw new Exception("Insert source dir exception; path = " + _dir.Value); }
           
        }

        private void InsertTaskTime(ScheduleTask _task)
        {
          
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO TASKTIME(NextDate, DayOfWeek, TaskID) VALUES(@Date,@DayOfWeek,@ID)";
                cmd.Parameters.AddWithValue("@Date", _task.NextDate.Ticks);
                cmd.Parameters.AddWithValue("@DayOfWeek", (int)_task.WeekDay);
                cmd.Parameters.AddWithValue("@ID", _task.Parent.ID);
                cmd.ExecuteNonQuery();
                _task.ID = SelectLastInsertedID("tasktime");
            }
            catch { throw new Exception("Insert task time exception;"); }
           
        }

        private void InsertBackup(Backup _backup)
        {
      
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO mBACKUP(Name,mDesc,Compress,TargetFoldID) VALUES(@Name,@Desc,@Compress,@TFoldID)";
                cmd.Parameters.AddWithValue("@Name", _backup.Name);
                cmd.Parameters.AddWithValue("@Desc", _backup.Description);
                cmd.Parameters.AddWithValue("@Compress", _backup.IsArchive.ToString());
                cmd.Parameters.AddWithValue("@TFoldID", _backup.TargetDir.ID);
                cmd.ExecuteNonQuery();
                _backup.ID = SelectLastInsertedID("mbackup");
            }
            catch { throw new Exception("Insert backup exception"); }
        }

        public void AddBackup(Backup _backup)
        {
            InsertTargetFolder(_backup.TargetDir);
            InsertBackup(_backup);

            foreach (BackupDir dir in _backup.SourceDirs) { InsertSourceDir(dir); }
            foreach (ScheduleTaskGroup group in _backup.ScheduleGroups)
            {
                InsertGroupTask(group);
                foreach (ScheduleTask task in group.Tasks) { InsertTaskTime(task); }
            }
        }

        private long InsertFolder(string _path, long _sourceFoldID)
        {

            var cmd = m_connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO FOLDER(Path,SourceFoldID) VALUES(@Path,@SourceID);";
            cmd.Parameters.AddWithValue("@Path", _path);
            cmd.Parameters.AddWithValue("@SourceID", _sourceFoldID);
            cmd.ExecuteNonQuery();
 
            return SelectLastInsertedID("folder");
        }

        public void InsertFilesFromTmp()
        {
  
            var cmd = m_connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO mFILE(Name,ModDate,FoldID)
                               SELECT Name,ModDate,FoldID FROM TMPFILE
                                ";
            cmd.ExecuteNonQuery();
 
        }

        public void AddNewFilesToTmp(ref List<FileInfo> _files, long _sourceID, string _path)
        {
            List<FileInfo> result = new List<FileInfo>();
            foreach (FileInfo file in _files)
            {
                try
                {
                    long foldID = this.SelectFolderID(_path,_sourceID);
                    long fileID = -1;
                    if (foldID > 0)
                    {
                        long ticks = this.SelectFileDate(ref fileID, file.Name, foldID);
                        if (ticks > 0)
                        {
                            if (file.LastWriteTime == new DateTime(ticks))
                                continue;
                        }
                    }
                    result.Add(file);

                    var cmd = m_connection.CreateCommand();
                    cmd.CommandText = @"INSERT INTO TMPFILE(Name,ModDate,FoldPath,FoldID,FileID,SourceFoldID) 
                                        VALUES(@Name,@Date,@Path,@FoldID,@FileID,@SourceID)";
                    cmd.Parameters.AddWithValue("@Name", file.Name);
                    cmd.Parameters.AddWithValue("@Date", file.LastWriteTime.Ticks);
                    cmd.Parameters.AddWithValue("@Path", _path);
                    cmd.Parameters.AddWithValue("@FoldID", foldID);
                    cmd.Parameters.AddWithValue("@FileID", fileID);
                    cmd.Parameters.AddWithValue("@SourceID", _sourceID);
                        cmd.ExecuteNonQuery();

                }
                catch { throw new Exception("Insert file to tmp table exception"); }
            }
            _files = result;
        }

        public void AddFilesToTmp(List<FileInfo> _files, long _sourceID, string _path)
        {
            try
            {
                foreach (FileInfo file in _files)
                {
                    var cmd = m_connection.CreateCommand();
                    cmd.CommandText = @"INSERT INTO TMPFILE(Name,ModDate,FoldPath,FoldID,FileID,SourceFoldID) 
                                        VALUES(@Name,@Date,@Path,-1,-1,@SourceID)";
                    cmd.Parameters.AddWithValue("@Name", file.Name);
                    cmd.Parameters.AddWithValue("@Date", file.LastWriteTime.Ticks);
                    cmd.Parameters.AddWithValue("@Path", _path);
                    cmd.Parameters.AddWithValue("@SourceID", _sourceID);
                    cmd.ExecuteNonQuery();
                }
            }
            catch { throw new Exception("Insert file to tmp table exception"); }   
        }
        public void AddDeleteBackup(string _path, DateTime _time)
        {
            try 
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO REMOVEITEMS(Path, DeleteDate) VALUES(@Path, @Date)";
                cmd.Parameters.AddWithValue("@Path", _path);
                cmd.Parameters.AddWithValue("@Date", _time.Ticks);
                cmd.ExecuteNonQuery();
            } catch
            {
                Log.Add("не удалась вставка в таблицу удаления");
            }
        }
    }
 
    
}
