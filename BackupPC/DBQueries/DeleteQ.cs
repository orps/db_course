﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
//using Finisar.SQLite;
using System.Data.SqlServerCe;
using System.Text.RegularExpressions;
using System.Threading;

namespace BackupPC
{
    public partial class DBQuery
    {

        public void DeleteBackupFromBase(Backup _backup)
        {

            /*foreach (ScheduleTaskGroup g in _backup.ScheduleGroups)
            {
                DeleteGroupTask(g.ID);
            }

            foreach (BackupDir d in _backup.SourceDirs)
            {
                DeleteSourceFolder(d.ID);
            }

            DeleteTargetFolder(_backup.TargetDir.ID);*/
            DeleteBackup(_backup.ID);
        }

        public void ClearTempTable()
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM TMPFILE"; 
                cmd.ExecuteNonQuery();
            }
            catch { throw new Exception("Clear tmpfile table exception"); }
        }

        public void ClearFileFolderTables(Backup _backup)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM mFILE WHERE FoldID 
                                        in (SELECT f.ID FROM FOLDER as f INNER JOIN SOURCEFOLDER as sf 
                                            on f.SourceFoldID = sf.ID WHERE sf.BackupID = @ID);";
                cmd.Parameters.AddWithValue("@ID", _backup.ID);
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM FOLDER WHERE SourceFoldID in (SElECT ID FROM SOURCEFOLDER WHERE BackupID = @ID);";
                cmd.Parameters.AddWithValue("@ID", _backup.ID);
            }
            catch { throw new Exception("Clear filefolder tables exception"); }
        }

        private void DeleteSourceFolder(long _id)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"
                    DELETE FROM SOURCEFOLDER WHERE ID = @ID; ";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"
                    DELETE FROM mFILE WHERE FoldID in (SELECT ID FROM FOLDER WHERE SourceFoldID=@ID); ";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"
                    DELETE FROM FOLDER WHERE SourceFoldID=@ID;";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
            }
            catch { throw new Exception("Delete source folder exception"); }
        }

        private void DeleteGroupTask(long _id)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM TASK WHERE ID = @ID; DELETE FROM TASKTIME WHERE TASKID = @ID;";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
            }
            catch { throw new Exception("Delete group task exception;"); }
        }

        private void DeleteFolderFiles(long _folderid)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM FILE WHERE FoldID = @ID;";
                cmd.Parameters.AddWithValue("@ID", _folderid);
                cmd.ExecuteNonQuery();
            }
            catch
            { 
                throw new Exception("Delete folder files exception;"); 
            }
        }

        private void DeleteTargetFolder(long _id)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM TARGETFOLDER WHERE ID = @ID;";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Delete target folder exception;");
            }
        }

        private void DeleteBackup(long _id)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM TARGETFOLDER WHERE ID in (SELECT TargetFoldID FROM mBACKUP WHERE ID = @ID);
                                    " ;
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();

                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM mBACKUP WHERE ID = @ID;";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM TASKTIME WHERE TaskID 
                                        in (SELECT ID FROM TASK WHERE BackupID = @ID);";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM TASK WHERE ID in (SELECT ID FROM TASK WHERE BackupID = @ID);
                                    ";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM mFILE WHERE FoldID 
                                        in (SELECT f.ID FROM FOLDER as f INNER JOIN SOURCEFOLDER as sf 
                                            on f.SourceFoldID = sf.ID WHERE sf.BackupID = @ID);
                                    ";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM FOLDER WHERE SourceFoldID in (SElECT ID FROM SOURCEFOLDER WHERE BackupID = @ID);
                                    ";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
                cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DElETE FROM SOURCEFOLDER WHERE BackupID = @ID; 
                                    ";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();

            }
            catch
            {
                throw new Exception("Delete backup exception;");
            }
        }

        private void DeleteFolder(long _id)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM FOLDER WHERE ID = @ID;";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Delete folder exception;");
            }
        }

        public void DeleteFileFromTmp(string _name, string _relPath, long _sourceID)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM TMPFILE WHERE 
                                    Name = @Name AND FoldPath = @Path AND SourceFoldID = @ID;";
                cmd.Parameters.AddWithValue("@Name", _name);
                cmd.Parameters.AddWithValue("@Path", _relPath);
                cmd.Parameters.AddWithValue("@ID", _sourceID);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Delete tmp file exception;");
            }
        }

        private void DeleteFile(long _id)
        {
            try
            {
                var cmd = m_connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM mFILE WHERE ID = @ID;";
                cmd.Parameters.AddWithValue("@ID", _id);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Delete file exception;");
            }
        }
    }
}
