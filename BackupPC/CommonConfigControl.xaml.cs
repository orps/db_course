﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BackupPC
{
    /// <summary>
    /// Interaction logic for ConfigBackupCommon.xaml
    /// </summary>
    public partial class CommonConfigControl : UserControl
    {
        public CommonConfigControl()
        {
            InitializeComponent();
        }

        public CommonConfigControl(Backup _backup)
        {
            InitializeComponent();
            boxBackupName.Text = _backup.Name;
            boxBackupDesc.AppendText(_backup.Description);
            cboxUseZip.IsChecked = _backup.IsArchive;
        }

    }
}
