﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BackupPC
{
    /// <summary>
    /// Interaction logic for DirConfigControl.xaml
    /// </summary>
    public partial class DirConfigControl : UserControl
    {
        private List<BackupDir> m_sourcePaths = new List<BackupDir>();
        private BackupDir m_targetPath;

        public BackupDir[] SourcePaths { get { return m_sourcePaths.ToArray(); } }
        public BackupDir TargetPath { get { return m_targetPath; } }

        public DirConfigControl()
        {
            InitializeComponent();
            dataSource.ItemsSource = m_sourcePaths;
        }

        public DirConfigControl(Backup _backup)
        {
            InitializeComponent();
            m_sourcePaths = _backup.SourceDirs.ToList();
            dataSource.ItemsSource = m_sourcePaths;
            m_targetPath = _backup.TargetDir;
            boxTargetFolder.Text = m_targetPath.Value;
        }

        // добавляем новую папочку-источник
        private void btnAddSource_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            var result = dialog.ShowDialog();
            dialog.Description = "Укажите директорию";
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string folder = dialog.SelectedPath;
                m_sourcePaths.Add(new BackupDir(folder));
                dataSource.ItemsSource = m_sourcePaths.ToArray();
            }
        }
        
        // удаляем папочку-источник
        private void btnRemoveSource_Click(object sender, RoutedEventArgs e)
        {
            int s = dataSource.SelectedIndex;
            if (s >= 0)
            {
                m_sourcePaths.Remove((BackupDir)dataSource.SelectedItem);
                dataSource.ItemsSource = m_sourcePaths.ToArray();
            }
            else
            {
                ErrorBox.Show("Не выбран ни один элемент в таблице");
            }

        }

        // добавляем новую конечную папку
        private void btnAddTarger_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            var result = dialog.ShowDialog();
            dialog.Description = "Укажите директорию";
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                m_targetPath = new BackupDir(dialog.SelectedPath);
                boxTargetFolder.Text = m_targetPath.Value;
            }
        }

    }
}
