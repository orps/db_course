﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hardcodet.Wpf.TaskbarNotification;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace BackupPC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    ///
    public static class ErrorBox
    {
        public static void Show(string _msg)
        {
            MessageBox.Show(_msg, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }

    public class BackupMenuItem : MenuItem
    {
        private Backup m_backup;

        public Backup Backup { get { return m_backup; } }

        public BackupMenuItem(Backup _backup)
        {
            m_backup = _backup;
        }
    }

    public class ScheduleGroupMenuItem : MenuItem
    {
        private ScheduleTaskGroup m_group;
        public ScheduleGroupMenuItem(ScheduleTaskGroup _group)
        {
            m_group = _group;
            string header = "Полный";
            if (_group.Type == BackupType.Differential)
                header = "Дифференциальный";
            else if (_group.Type == BackupType.Incremental)
                header = "Инкрементальный";
            this.Header = header;
        }
        public ScheduleTaskGroup ScheduleGroup { get { return m_group; } }
    }

    public partial class MainWindow : Window    
    {
        TaskbarIcon tb = new TaskbarIcon();
        //recreate new db
        private bool m_isCreateDB = true;

        //dont touch it in code, you only can iterate through it, use AddBackup and DeleteBackup
        private List<Backup> m_backups;
        private TaskQueue m_taskQueue;

        public MainWindow()
        {
            InitializeComponent();
            CreateNotify();

            
            Log.Init(this.boxLog);

            m_backups = new List<Backup>();
            m_taskQueue = new TaskQueue();
            if (File.Exists(DBQuery.DBName))
            {
                if (LoadDB())
                {
                    LoadRemoveCandidates();
                }
                else
                    CreateDB();
            }
            else
            {
                CreateDB();
            }
            StartQueue();
        }

        private void CreateDB()
        {
            Log.Add("Создается новая база данных");
            DBQuery query = null;
            try
            {
                query = new DBQuery(true);
                query.BeginTransaction();
                query.CreateDB();
                query.CommitTransaction(true);
            }
            catch
            {
                query.RollbackTransaction(true);
                ErrorBox.Show("Не удалось создать базу данных");
                Application.Current.Shutdown();
            }
        }

        private bool LoadDB()
        {
            bool result = false;
            DBQuery query = new DBQuery();
            try
            {
                List<Backup> backups = query.Load();
                if (query.IsAnyActive())
                {
                    query.BeginTransaction();
                    CheckUnfinishedBackups(backups, query);
                    query.CommitTransaction();
                }
                result = true;
                foreach (Backup b in backups)
                {
                    AddBackupToList(b);
                }
                Log.Add("База данных проинициализирована");
            }
            catch {}
            query.CloseConnection();

            return result;
        }

        private void LoadRemoveCandidates()
        {
            
        }

        private void CheckUnfinishedBackups(List<Backup> _list, DBQuery _query)
        {
            foreach (Backup b in _list)
            {
                foreach (ScheduleTaskGroup g in b.ScheduleGroups)
                {
                    if (g.Status != BackupStatus.Finished)
                    {
                        Log.Add("Бэкап " + b.Name + " был прерван");
                        g.Status = BackupStatus.Finished;
                        _query.UpdateStatus(g);
                    }
                }
            }
        }

        private void StartQueue()
        {
            Thread t = new Thread(m_taskQueue.Run);
            t.Start();
        }

        private void UpdateMenu()
        {
            menuItemBackupChange.Items.Clear();
            menuItemBackupDelete.Items.Clear();
            menuItemBackupRun.Items.Clear();

            foreach (Backup b in m_backups)
            {
                MenuItem item = new BackupMenuItem(b);
                item.Header = b.Name;
                item.Click += menuItemBackupChange_Click;
                menuItemBackupChange.Items.Add(item);

                item = new BackupMenuItem(b);
                item.Header = b.Name;
                item.Click += menuItemBackupDelete_Click;
                menuItemBackupDelete.Items.Add(item);

                item = new MenuItem();
                item.Header = b.Name;
                InitMenuTasks(b, item);
                menuItemBackupRun.Items.Add(item);
            } 
        }

        private void AddBackupToList(Backup _b)
        {
            m_backups.Add(_b);
            List<ITask> taskList = new List<ITask>() ;
            foreach (ScheduleTaskGroup g in _b.ScheduleGroups)
            {
                foreach (ScheduleTask t in g.Tasks)
                {
                    taskList.Add(new BackupTask(t));
                }
            }
            m_taskQueue.Add(taskList.ToArray());
            this.UpdateMenu(); 
        }

        private void RemoveBackupFromList(Backup _b)
        {
            m_backups.Remove(_b);
            m_taskQueue.Remove(_b.ID);
            this.UpdateMenu();
        }

        private void RemoveBackup(Backup _b)
        {
            
            DBQuery query = new DBQuery();
            try
            {
                query.BeginTransaction();
                query.DeleteBackupFromBase(_b);
                query.CommitTransaction(true);
                RemoveBackupFromList(_b);
            }
            catch { query.RollbackTransaction(true); }
        }

        private void CreateNotify()
        {
            tb.Icon = new System.Drawing.Icon("icon.ico");
            tb.Visibility = Visibility.Visible;
            ContextMenu menu = new ContextMenu();

            MenuItem item = new MenuItem();
            item.Header = "Open";
            item.Click += MenuSetVisibleWindow;
            menu.Items.Add(item);

            item = new MenuItem();
            item.Header = "Close";
            item.Click += MenuCloseWindow;
            menu.Items.Add(item);

            tb.ContextMenu = menu;
        }

        public void MenuSetVisibleWindow(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Visible;
        }

        public void MenuCloseWindow(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            //uncomment when ready
            //e.Cancel = true;
        }

        private void menuItemExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void menuItemBackupAdd_Click(object sender, RoutedEventArgs e)
        {
            BackupConfig b = new BackupConfig();
            b.ShowDialog();
            if (b.isOk) { this.AddBackupToList(b.Backup); }
        }

        private void menuItemBackupChange_Click(object sender, RoutedEventArgs e)
        {

            if (sender is BackupMenuItem)
            {
                var item = sender as BackupMenuItem;
                bool flag;
                item.Backup.Enter();
                flag = item.Backup.IsActive();
                if (false == flag)
                    item.Backup.SetEditing();
                item.Backup.Release();
                if (false == flag) {
                    BackupConfig b = new BackupConfig(item.Backup);
                    b.ShowDialog();
                    if (b.isOk) { RemoveBackupFromList(item.Backup); AddBackupToList(b.Backup); }
                    // insert here code to delete from queue and add there new tasks
                    b.Backup.ResetEditing();
                    item.Backup.ResetEditing();
                }
                else
                    ErrorBox.Show("Элемент в данный момент задействован");
            }

        }

        private void menuItemBackupDelete_Click(object sender, RoutedEventArgs e)
        {
            if (sender is BackupMenuItem)
            {
                var item = sender as BackupMenuItem;
                Backup b = item.Backup;
                b.Enter();
                bool flag = true;
                foreach (ScheduleTaskGroup g in b.ScheduleGroups)
                {
                    if (g.Status != BackupStatus.Finished)
                        flag = false;
                }
                if (true)
                {
                    this.RemoveBackup(b);  b.Release();
                    Log.Add("Бэкап " + b.Name +  " удален");
                }
                else
                {
                    b.Release();
                    ErrorBox.Show("Элемент в данный момент задействован");
                }
                
            }
        }

        private void InitMenuTasks(Backup _backup, MenuItem _parent)
        {
            bool flagFull = false;
            bool flagIncr = false;
            bool flagDiff = false;

            foreach (ScheduleTaskGroup _group in _backup.ScheduleGroups)
            {
                if (_group.Type == BackupType.Full && !flagFull)
                {
                    MenuItem item = new ScheduleGroupMenuItem(_group);
                    flagFull = true;
                    item.Click += menuItemBackupRun_Click;
                    _parent.Items.Add(item);
                }
                else if (_group.Type == BackupType.Incremental && !flagIncr)
                {
                    MenuItem item = new ScheduleGroupMenuItem(_group);
                    flagIncr = true;
                    item.Click += menuItemBackupRun_Click;
                    _parent.Items.Add(item);
                }
                else if (_group.Type == BackupType.Differential && !flagDiff)
                {
                    MenuItem item = new ScheduleGroupMenuItem(_group);
                    flagDiff = true;
                    item.Click += menuItemBackupRun_Click;
                    _parent.Items.Add(item);
                }
            }
        }

        private void menuItemBackup_SubmenuOpened(object sender, RoutedEventArgs e)
        {

        }


        private void menuItemBackupRun_Click(object sender, RoutedEventArgs e)
        {
            var obj = sender as ScheduleGroupMenuItem;
            ScheduleTaskGroup g = obj.ScheduleGroup;
            ITask itask = new BackupTask(g);
            m_taskQueue.Add(itask);
            Log.Add("Задача добавлена в очередь");

        }

        private void menuItemBackup_SubmenuClosed(object sender, RoutedEventArgs e)
        {
        }

        private void Menu_ContextMenuOpening_1(object sender, ContextMenuEventArgs e)
        {
        }

        private void menuItemBackup_Click(object sender, RoutedEventArgs e)
        {
           
        }


    }
}
