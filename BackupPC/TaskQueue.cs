﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BackupPC
{
    public interface ITask
    {
        void Run(bool _isExpired);
        ITask[] Update();
        DateTime Time { get; }
        bool IsBelongsToBackup(long _id);
    }

    public class BackupTask: ITask 
    {
        DateTime m_runTime;
        bool m_isSingle;
        ScheduleTaskGroup m_group;
        ScheduleTask m_task;
        Backup m_backup;
        List<ITask> m_nextTasks;
        string m_deletePath = "";

        public DateTime Time { get { return m_runTime; } }

        public bool IsBelongsToBackup(long _id)
        {
            return m_backup.ID == _id;
        }

        public BackupTask(ScheduleTaskGroup _group)
        {
            m_nextTasks = new List<ITask>();
            m_task = null;
            m_group = _group;
            m_backup = _group.Backup;
            m_runTime = DateTime.Now;
            m_isSingle = true;
            m_task = _group.Tasks[0];
        }

        public BackupTask(ScheduleTask _task)
        {
            m_nextTasks = new List<ITask>();
            m_task = _task;
            m_group = _task.Parent;
            m_backup = m_group.Backup;
            m_runTime = _task.NextDate;
            m_isSingle = false;
        }

        public void Run(bool _isExpired)
        {
            bool isAnyActive = false;
            string timePrefix = m_runTime.ToShortDateString();
            timePrefix += "_" + m_runTime.ToShortTimeString() + ":" + m_runTime.Second.ToString();
            DateTime taskTime = m_runTime;
            m_backup.Enter();
            if (_isExpired)
                Log.Add("Бэкап " + m_group.Backup.Name + " просрочен");
            if (m_backup.IsEditing)
                Log.Add("Бэкап " + m_group.Backup.Name + " не может быть выполнен, потому что открыто окно редактирования");

            if (false == m_backup.IsEditing)
            {
                DBQuery query = new DBQuery();
                query.BeginTransaction();
                isAnyActive = query.IsAnyActive();
                if (isAnyActive && !_isExpired)
                    Log.Add("Бэкап " + m_group.Backup.Name + " не может быть выполнен, потому что выполняется другой");
                if (false == m_isSingle)
                {
                    while (m_task.NextDate <= DateTime.Now)
                        m_task.IncrementDate();

                    m_nextTasks.Add(new BackupTask(m_task));
                    query.UpdateTaskRunTime(m_task);
                }
                if (isAnyActive == false)
                {
                    m_group.Status = BackupStatus.Running;
                    query.UpdateStatus(m_group);
                    Log.Add("Выполняется бэкап:" + m_group.Backup.Name);
                }
                query.CommitTransaction(true);
            }
            m_backup.Release();
            if (isAnyActive == true || _isExpired)
            {
               return;
            }

            AbsTask task = null;
            CopyMethod cmethod = null;
            if (m_backup.IsArchive)
                cmethod = new ArchiveCopyMethod();
            else
                cmethod = new FolderCopyMethod();
            if (m_group.Type == BackupType.Full)
            {
                cmethod.SetPrefix("full_"+timePrefix);
                task = new FullBackupTask(m_task, cmethod );
            }
            else if (m_group.Type == BackupType.Incremental)
            {
                cmethod.SetPrefix("inc_" + timePrefix);
                task = new OtherBackupTask(m_task, cmethod);
            }
            else if (m_group.Type == BackupType.Differential)
            {
                cmethod.SetPrefix("diff_" + timePrefix);
                task = new OtherBackupTask(m_task, cmethod);
            }
            DBQuery dbquery = new DBQuery();
            
            dbquery.BeginTransaction();
            try {
                task.Start(dbquery);
                m_group.Status = BackupStatus.Copied;
                dbquery.UpdateStatus(m_group);
                task.Finish(dbquery);
                m_group.Status = BackupStatus.Finished;
                dbquery.UpdateStatus(m_group);
                AddDelete(cmethod.m_container, dbquery);
                dbquery.CommitTransaction(true);
                string sizestr = "";
                long length = cmethod.CommonSize;
                if (length < 10240)
                    sizestr = length.ToString() + " Байт(а)";
                else if (length < 10240 * 1024)
                    sizestr = (length/1024).ToString() + "КБайт(а)";
                else 
                    sizestr = (length/(1024*1024)).ToString() + "МБайт(а)";
                string msg = "Бэкап " + m_group.Backup.Name + " выполнен;\nСкопировано " +
                    cmethod.FileCount.ToString() + " файл(a,ов) общим размером в " + sizestr;
                Log.Add(msg);
            } catch {
                dbquery.RollbackTransaction(true);
                dbquery = new DBQuery();
                dbquery.BeginTransaction();
                m_group.Status = BackupStatus.Finished;
                dbquery.UpdateStatus(m_group);
                dbquery.CommitTransaction(true);
                Log.Add("Выполнение бэкапа " + m_group.Backup.Name + " прервано из-за ошибки");
            }

        }

        private void AddDelete(string _path , DBQuery _query)
        {
            if (m_group.DeleteCopyAfter > 0)
            {
                TimeSpan span = new TimeSpan(m_group.DeleteCopyAfter, 0, 0, 0);
                DateTime time = m_runTime + span;
                m_nextTasks.Add(new DeleteTask(_path, time));
            }
        }

        public void Finish()
        {
        }

        public ITask[] Update()
        {
            return m_nextTasks.ToArray();
        }
    }

    public class DeleteTask : ITask
    {
        private string m_path;
        private DateTime m_time;
        public DeleteTask(string _path, DateTime _time)
        {
            m_path = _path;
            m_time = _time;
        }
        public void Run(bool _isExpired)
        {
            DBQuery query = new DBQuery();
            query.BeginTransaction();
            Log.Add("Удаление бэкапа: " + m_path);
            try
            {
                Directory.Delete(m_path, true);
                Log.Add("Бэкап: " + m_path + " удален");
            }
            catch
            {
                Log.Add("Бэкап: " + m_path + " не удален из-за ошибки");
            }
            query.CommitTransaction();
        }

        public ITask[] Update()
        {
            return new ITask[] { };
        }

        public DateTime Time { get { return m_time; } }
        public bool IsBelongsToBackup(long _id)
        {
            return false;
        }

    }

    public class TaskComparer : IComparer<ITask>
    {
        public int Compare(ITask a, ITask b)
        {
            if (a == null)
            {
                if (b == null) { return 0; }
                else { return -1; }
            }
            else
            {
                if (b == null) { return 1; }
                else { return a.Time.CompareTo(b.Time); }
            }
        }
    }

    public class TaskQueue
    {
        private List<ITask> m_taskList;
        private Mutex m_mutex;

        public TaskQueue()
        {
            m_mutex = new Mutex();
            m_taskList = new List<ITask>();
        }

        private void Enter()
        {
            m_mutex.WaitOne(-1);
        }

        private void Release()
        {
            m_mutex.ReleaseMutex();
        }

        private void ThreadStart(object _task)
        {
            var task = _task as ITask;
            Remove(task);
            bool isExpired = DateTime.Now - task.Time > new TimeSpan(0, 1, 0);
            task.Run(isExpired);
            ITask[] newTasks = task.Update();
            this.Add(newTasks);
        }

        public void Run()
        {
            while (true)
            {
                
                this.Enter();
                if (m_taskList.Count > 0)
                {
                    ITask task = m_taskList.ElementAt(0);
                    DateTime time = task.Time;
                    if (time <= DateTime.Now)
                    {
                        Thread t = new Thread(this.ThreadStart);
                        t.Start((object)task);
                    }
                }
                this.Release();
                Thread.Sleep(500);
            }
        }

        public void Add(ITask[] _tasks)
        {
            this.Enter();
            foreach (ITask task in _tasks)
            {
                m_taskList.Add(task);
            }
            m_taskList.Sort(new TaskComparer());
            this.Release();
        }

        public void Add(ITask _task)
        {
            this.Enter();
            m_taskList.Add(_task);
            m_taskList.Sort(new TaskComparer());
            this.Release();
        }
        public void Remove(ITask _task)
        {
            this.Enter();
            m_taskList.Remove(_task);
            this.Release();
        }

        public void Remove(long _backupID)
        {
            this.Enter();
            ITask[] removeCandiates = m_taskList.Where(x => x.IsBelongsToBackup(_backupID)).ToArray();
            foreach (ITask t in removeCandiates)
            {
                m_taskList.Remove(t);
            }
            this.Release();
        }
    }
}
