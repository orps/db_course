﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BackupPC
{
    /// <summary>
    /// Interaction logic for ScheduleConfigControl.xaml
    /// </summary>
    public partial class ScheduleConfigControl : UserControl
    {
        //wrapper for ScheduleTaskGroup for display in datagrid
        private class ScheduleGroupWrapper
        {
            private static string[] m_types = {"Полный", "Инкрементальный", "Диференциальный"};
            private static string[] m_days = new string[] {
                "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"
            };

            ScheduleTaskGroup m_schedule;
            public ScheduleGroupWrapper(ScheduleTaskGroup _schedule)
            {
                m_schedule = _schedule;
            }

            public ScheduleTaskGroup Schedule { get { return m_schedule; } }

            public string Type { get { return m_types[(int)m_schedule.Type]; } }

            public string Days 
            { 
                get 
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (ScheduleTask task in m_schedule.Tasks)
                    {
                        sb.Append(m_days[(int)task.WeekDay] + ", ");
                    }
                    sb.Remove(sb.Length - 2, 2);
                    return sb.ToString();
                } 
            }

            public string Time { get {
                return ((m_schedule.Hours < 10 ? ("0" + m_schedule.Hours.ToString()) : m_schedule.Hours.ToString()) + ":" +
                    (m_schedule.Minutes < 10 ? ("0" + m_schedule.Minutes.ToString()) : m_schedule.Minutes.ToString())); 
            } }
        }


        // расписания для таблицы
        private List<ScheduleGroupWrapper> m_schedules;

        public ScheduleTaskGroup[] Schedule { get { return m_schedules.Select(x => x.Schedule).ToArray(); } }

        //новое расписание
        public ScheduleConfigControl()
        {
            InitializeComponent();
            m_schedules = new List<ScheduleGroupWrapper>();
        }

        // изменить расписание уже созданного бэкапа
        public ScheduleConfigControl(Backup _backup)
        {
            InitializeComponent();
            m_schedules = _backup.ScheduleGroups.Select(x => new ScheduleGroupWrapper(x)).ToList();
            dataSchedule.ItemsSource = m_schedules;
        }

        //добавить новое расписание в таблицу
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            ScheduleWindow w = new ScheduleWindow();
            w.ShowDialog();
            if (w.IsOk)
            {
                m_schedules.Add(new ScheduleGroupWrapper(w.Schedule));
                dataSchedule.ItemsSource = m_schedules.ToArray();
            }
        }

        //отобразить формочку со всеми свойствами выбранного в таблице расписания 
        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            if (dataSchedule.SelectedIndex < 0)
            {
                ErrorBox.Show("Не выбрано ни одно расписание в таблице");
            }
            else
            {
                ScheduleWindow w = new ScheduleWindow(((ScheduleGroupWrapper)dataSchedule.SelectedItem).Schedule);
                w.ShowDialog();   
            }
        }

        //delete schedule from table
        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            int item = dataSchedule.SelectedIndex;
            if (item < 0)
            {
                ErrorBox.Show("Не выбрано ни одно расписание в таблице");
            }
            else
            {
                m_schedules.RemoveAt(item);
                dataSchedule.ItemsSource = m_schedules.ToArray();
            }
        }
    }
}
