﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BackupPC
{
    /// <summary>
    /// Interaction logic for BackupConfig.xaml
    /// </summary>
    /// 
    public partial class BackupConfig : Window
    {
        // class for wrapper of user controls for using in datagrid
        private class ConfigItem
        {
            protected string m_name;
            public string ConfigName { get { return m_name; } }
            public UserControl m_control;

            public ConfigItem(string _name, UserControl _control)
            {
                m_name = _name;
                m_control = _control;
               // m_control.Margin = new Thickness(150, 0, 0, 0);
                m_control.VerticalAlignment = VerticalAlignment.Top;
                m_control.HorizontalAlignment = HorizontalAlignment.Left;
                m_control.Visibility = Visibility.Hidden;
            }

            public UserControl GetControl()
            {
                return m_control;
            }
        }

        public Backup Backup { get { return m_backup; } }
        public bool isOk { get { return m_isOK; } }
        
        //
        private Backup m_backup;
        private bool m_isOK = false;
        private bool m_isCreate;
        
        // list of wrappers of user controls for datagrid
        private ConfigItem[] m_configList;

        // current index of visible control from m_configList 
        int m_currentVisibleUserControl = -1;

        private CommonConfigControl m_commonControl;
        private DirConfigControl m_dirControl;
        private ScheduleConfigControl m_scheduleControl;

        //init new backup
        public BackupConfig()
        {
            InitializeComponent();
            m_commonControl = new CommonConfigControl();
            m_dirControl = new DirConfigControl();
            m_scheduleControl = new ScheduleConfigControl();
            m_isCreate = true;
            this.InitConfigDataGrid();
        }

        //change backup
        public BackupConfig(Backup _backup)
        {
            InitializeComponent();
            m_isCreate = false;
            m_backup = (Backup)_backup.Clone();
            m_commonControl = new CommonConfigControl(m_backup);
            m_dirControl = new DirConfigControl(m_backup);
            m_scheduleControl = new ScheduleConfigControl(m_backup);
            m_commonControl.boxBackupName.IsEnabled = false;

            this.InitConfigDataGrid();
        }

        // общая часть конструкторов
        private void InitConfigDataGrid()
        {
            m_configList = new ConfigItem[] {
                new ConfigItem("Общие", m_commonControl),
                new ConfigItem("Директории", m_dirControl),
                new ConfigItem("Расписание", m_scheduleControl)
            };

            dataConfMenu.SelectionMode = DataGridSelectionMode.Single;
            dataConfMenu.Focus();
            dataConfMenu.SelectedIndex = 0;
            foreach (ConfigItem c in m_configList)
            {
                //MyGrid.Children.Add(c.GetControl());
                Grid.SetColumn(c.GetControl(), 1);
                Grid.SetRow(c.GetControl(), 1);
                MyGrid.Children.Add(c.GetControl());
            }
            dataConfMenu.ItemsSource = m_configList;
        }

        // switch user controls
        private void dataConfMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_currentVisibleUserControl >= 0)
                m_configList[m_currentVisibleUserControl].GetControl().Visibility = Visibility.Hidden;
            
            var d = sender as DataGrid;
            m_currentVisibleUserControl = d.SelectedIndex;
            ((ConfigItem)d.SelectedItem).GetControl().Visibility = Visibility.Visible;
        }
        
        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //foreach (ConfigItem c in m_configList)
            //    MyGrid.Children.Remove(c.GetControl());
        }

        //cancel
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //apply changes or create backup and make changes in db
        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            DBQuery query = null;
            try
            {
                CheckName();
                CheckDirs();
                CheckSchedule();
                //db_apply
                query = new DBQuery();
                query.BeginTransaction();
                if (m_isCreate) { CreateBackup(query); }
                else { ChangeBackup(query); }
                //
                query.CommitTransaction(true);
                m_isOK = true;
                this.Close();
            }
            catch(Exception ex)
            {
                ErrorBox.Show(ex.Message);
                if (query != null) { query.RollbackTransaction(true); this.Close(); }
            }
        }
    }

    //final create part
    public partial class BackupConfig : Window
    {
        //change backup in db
        public void ChangeBackup(DBQuery _query)
        {   
            RichTextBox rtb = m_commonControl.boxBackupDesc;
            string desc = (new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd)).Text;
            //description
            if (m_backup.Description != desc)
            {
                m_backup.Description = desc;
                _query.UpdateBackupDescription(m_backup);
            }

            //archive option
            if (m_backup.IsArchive != (bool)m_commonControl.cboxUseZip.IsChecked)
            {
                m_backup.IsArchive ^= true;
                _query.UpdateBackupArchive(m_backup);
            }

            //sourcefolders
            int count = 0;
            foreach(BackupDir dir in m_dirControl.SourcePaths)
            {
                if (dir.ID == -1) 
                {
                    count++;
                    dir.Backup = m_backup;
                }
            }

            // if there is new backups or some backups removed
            if (count > 0 || m_dirControl.SourcePaths.Length != m_backup.SourceDirs.Length)
            {
                m_backup.SourceDirs = m_dirControl.SourcePaths;
                _query.UpdateSourceFolders(m_backup);
            }

            //targetfolder
            if (m_backup.TargetDir.Value != m_dirControl.TargetPath.Value)
            {
                m_backup.TargetDir.Value = m_dirControl.TargetPath.Value;
                m_backup.TargetDir.Backup = m_backup;
                _query.UpdateTargetFolder(m_backup);
            }

            //schedule
            count = 0;
            foreach(ScheduleTaskGroup group in m_scheduleControl.Schedule)
            {
                if (group.ID == -1) { count++; group.Backup = m_backup; }
            }

            // if there is new schedules or some removed then refresh 
            if (count > 0 || m_scheduleControl.Schedule.Length != m_backup.ScheduleGroups.Length)
            {
                m_backup.ScheduleGroups = m_scheduleControl.Schedule;
                _query.UpdateSchedule(m_backup);
            }
        }

        public void CreateBackup(DBQuery _query)
        {
            m_backup = new Backup();
            m_backup.IsArchive = (bool)m_commonControl.cboxUseZip.IsChecked;
            m_backup.Name = m_commonControl.boxBackupName.Text;
            RichTextBox rtb = m_commonControl.boxBackupDesc;
            m_backup.Description = (new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd)).Text;
            m_backup.SourceDirs = m_dirControl.SourcePaths;
            m_backup.TargetDir = m_dirControl.TargetPath;
            m_backup.ScheduleGroups = m_scheduleControl.Schedule;
            m_backup.InitParentForAllChildrens();
            _query.AddBackup(m_backup);
        }
    }

    //check part
    public partial class BackupConfig : Window
    {
        private void CheckName()
        {
            if ((m_commonControl.boxBackupName.Text.Trim()).Length == 0)
            {
                dataConfMenu.SelectedIndex = 0;
                m_commonControl.boxBackupName.Focus();
                throw new Exception("Не задано имя");
            }
        }

        private void CheckDirs()
        {
            if (m_dirControl.dataSource.Items.Count == 0)
            {
                dataConfMenu.SelectedIndex = 1;
                m_dirControl.btnAddSource.Focus();
                throw new Exception("Не задана ни одна исходная директория");
            }
            if (m_dirControl.boxTargetFolder.Text.Length == 0)
            {
                dataConfMenu.SelectedIndex = 1;
                m_dirControl.btnAddTarger.Focus();
                throw new Exception("Не задана конечная директория");
            }
        }

        private void CheckSchedule()
        {
            if (m_scheduleControl.dataSchedule.Items.Count == 0)
            {
                dataConfMenu.SelectedIndex = 2;
                m_scheduleControl.btnAdd.Focus();
                throw new Exception("Не задано расписание");
            }
        }

    }
}
