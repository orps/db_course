﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace BackupPC
{
    public enum BackupType
    {
        Full,
        Incremental,
        Differential
    }

    public enum BackupStatus
    {
        Running,
        Copied,
        Finished
    }

    // таск - атомарный элемент бэкапа(как иерархии) отвечает за один день недели
    public class ScheduleTask: ICloneable
    {
        public long ID { get; set; }
        public ScheduleTaskGroup Parent { get { return m_parent; } }
        public DateTime NextDate { get { return m_nextDate; } } //время и дата следующего запуска
        public DayOfWeek WeekDay { get { return m_day; } }

        public ScheduleTask(ScheduleTaskGroup _parent,DayOfWeek _day, DateTime _date)
        {
            ID = -1;
            m_parent = _parent;
            m_day = _day;
            m_nextDate = _date;
        }

        private ScheduleTask()
        { }

        //инкрементим на неделю, 2 .. - время следующего запуска
        public void IncrementDate()
        {
            m_nextDate = m_nextDate.AddDays(Parent.RepeatAfterWeek*7);
        }

        public object Clone()
        {
            return this.MemberwiseClone();            
        }

        public object Clone(ScheduleTaskGroup _parent)
        {
            ScheduleTask task = (ScheduleTask)this.MemberwiseClone();
            task.m_parent = _parent;
            return task;
        }

        private ScheduleTaskGroup m_parent;
        private DateTime m_nextDate;
        private DayOfWeek m_day;//день недели, в который выполняется этот таск
    }

    // группа тасков(берется бэкап и разбивается на таски и группируется с помощью этого класса)
    // отвечает за неделю
    public class ScheduleTaskGroup : ICloneable
    {
        public long ID { get; set; }
        public Backup Backup { get; set; }

        public ScheduleTask[] Tasks { get { return m_tasks; } }
        public BackupType Type { get { return m_type; } }
        public int Minutes { get { return m_minutes; } }
        public int Hours { get { return m_hours; } }
        public int RepeatAfterWeek { get { return m_repeatWeekTime; } }
        public int DeleteCopyAfter { get { return m_TTL; } }

        private BackupType m_type; 
        private int m_hours; //в какое время запускать бэкапы
        private int m_minutes;//
        private int m_TTL; // время жизни копий
        private int m_repeatWeekTime; // кол-во недель, через которое надо повторить бэкап
        private ScheduleTask[] m_tasks; // таски
        public BackupStatus Status;

        public void UpdateTasksAndMinHour(ScheduleTask[] _tasks)
        {
            m_tasks = _tasks;
            DateTime d = m_tasks[0].NextDate;
            m_minutes = d.Minute;
            m_hours = d.Hour;
        }

        public ScheduleTaskGroup(BackupType _type, DayOfWeek[] _days, int _repeat, int _ttl, int _h, int _m)
        {
            Status = BackupStatus.Finished;
            ID = -1;
            this.Backup = null;
            m_minutes = _m;
            m_hours = _h;
            m_TTL = _ttl;
            m_type = _type;
            m_repeatWeekTime = _repeat;
            CreateTasks(_days);
        }

        public ScheduleTaskGroup(long _id,  int _repeat, 
            int _ttl, BackupType _type, BackupStatus _stts)
        {
            ID = _id;
            m_repeatWeekTime = _repeat;
            m_TTL = _ttl;
            m_type = _type;
            Status = _stts;
        }

        public object Clone()
        {
            ScheduleTaskGroup res = (ScheduleTaskGroup)this.MemberwiseClone();
            res.m_tasks = m_tasks.Select(x => (ScheduleTask)x.Clone(res)).ToArray();
            return res;
        }

        public object Clone(Backup _parent)
        {
            ScheduleTaskGroup res = (ScheduleTaskGroup)this.MemberwiseClone();
            res.Backup = _parent;
            res.m_tasks = m_tasks.Select(x => (ScheduleTask)x.Clone(res)).ToArray();
            return res;
        }

        //создать дочерние таски, отвечающие за каждый заданный день недели и установить в них время время следующего запуска 
        private void CreateTasks(DayOfWeek[] _days)
        {
            DateTime today = DateTime.Today;
            List<ScheduleTask> list = new List<ScheduleTask>();
            foreach (DayOfWeek day in _days)
            {
                // генерация тасков и задание даты следующего запуска таска( дата и время запуска > текущей даты и времени)
                DateTime date = new DateTime(today.Year, today.Month, today.Day, m_hours, m_minutes, 0);
                //если текущий день недели больше дня недели таска
                if (day < today.DayOfWeek) {
                    date.AddDays(7 + (int)today.DayOfWeek - (int)day);
                }
                // если текущий день недели равен дню недели, в который должен быть запушен таск
                else if (day == today.DayOfWeek)
                {
                    if (today.Hour < m_hours || (today.Hour == m_hours && today.Minute <= m_minutes))
                        date.AddDays(7);
                }
                // если текущий день недели < дня недели, в который должен быть запущен таск
                else
                {
                    date.AddDays((int)day - (int)today.DayOfWeek);
                }
                list.Add(new ScheduleTask(this, day, date));
            }
            m_tasks = list.ToArray();
        }
    }

    // сам бэкап
    public class Backup: ICloneable
    {
        public long ID { get; set; }
        public BackupDir[] SourceDirs { get; set; }
        public BackupDir TargetDir { get; set; }
        public ScheduleTaskGroup[] ScheduleGroups {get;set;}
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsArchive { get; set; }
        private Mutex m_mutex = new Mutex();
        bool m_isEditing;

        public bool IsEditing { get { return m_isEditing; } }
        
        public void SetEditing()
        {
            m_isEditing = true;
        }

        public void ResetEditing()
        {
            m_isEditing = false;
        }

        public void Enter()
        {
            m_mutex.WaitOne();
        }

        public void Release()
        {
            m_mutex.ReleaseMutex();
        }

        public bool IsActive()
        {
            bool result;
            DBQuery query = new DBQuery();
            result = query.IsBackupActive(ID);
            query.CloseConnection();

            return result;
        }

        public Backup()
        {
            ID = -1;
        }

        public object Clone()
        {
            Backup res = (Backup)this.MemberwiseClone();
            res.SourceDirs = SourceDirs.Select(x => (BackupDir)x.Clone(res)).ToArray();
            res.TargetDir = (BackupDir)TargetDir.Clone(res);
            res.ScheduleGroups = ScheduleGroups.Select(x => (ScheduleTaskGroup)x.Clone(res)).ToArray();
            return res;
        }

        public void InitParentForAllChildrens()
        {
            foreach (BackupDir dir in SourceDirs) { dir.Backup = this; }
            TargetDir.Backup = this;
            foreach (ScheduleTaskGroup group in ScheduleGroups) { group.Backup = this; }
        }
    }

    //обертка для папочных путей
    public class BackupDir: ICloneable
    {
        public Backup Backup { get; set; }
        public long ID {get;set;}
        private string m_value;
        public string Value { get { return m_value; } set { m_value = value; } }
        public BackupDir(string _value)
        {
            this.Backup = null;
            m_value = _value;
            ID = -1;
        }

        public BackupDir(long _id, string _path)
        {
            ID = _id;
            m_value = _path;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public object Clone(Backup _parent)
        {
            object res = this.MemberwiseClone();
            ((BackupDir)res).Backup = _parent;
            return res;
        }
    }

}
