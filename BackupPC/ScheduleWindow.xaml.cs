﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BackupPC
{
    /// <summary>
    /// Interaction logic for ScheduleWindow.xaml
    /// </summary>
    public partial class ScheduleWindow : Window
    {
        private bool m_isOk = false;
        private ScheduleTaskGroup m_schedule; // само расписание
        public bool IsOk { get { return m_isOk; } } // создали ли мы расписание
        public ScheduleTaskGroup Schedule { get { return m_schedule; } }

        private class ScheduleException:Exception
        {
            string m_msg;

            public ScheduleException(string _msg) { m_msg = _msg; }

            public override string Message  { get { return m_msg; } }
        }

        // новое расписание
        public ScheduleWindow()
        {
            InitializeComponent();
            rbtnFull.IsChecked = true;
            cboxRepeatTime.SelectedIndex = 0;
            for (int i = 0; i < 31; i++)
            {
                cboxTTL.Items.Add(InitComboItem(i));
            }
            cboxTTL.SelectedIndex = 0;
        }

        // показать свойства уже созданного расписания
        public ScheduleWindow(ScheduleTaskGroup _group)
        {
            InitializeComponent();
            this.Title = "Свойства";
            ScheduleTask[] tasks = _group.Tasks;
            (new RadioButton[] { rbtnFull, rbtnIncr, rbtnDiff })[(int)_group.Type].IsChecked = true;
            CheckBox[] cboxs = new CheckBox[] { cboxSun, cboxMon, cboxTue, cboxThu, cboxFri, cboxSat };
            for (int i = 0; i < tasks.Length; i++)
            {
                cboxs[(int)tasks[i].WeekDay].IsChecked = true;
            }
            for (int i = 0; i < 31; i++) { cboxTTL.Items.Add(InitComboItem(i)); }
            cboxRepeatTime.SelectedIndex = _group.RepeatAfterWeek-1;
            cboxTTL.SelectedIndex = _group.DeleteCopyAfter;
            boxTime.Text = (_group.Hours < 10 ? ("0" + _group.Hours.ToString()) : _group.Hours.ToString()) + ":" + (_group.Minutes < 10 ? ("0" + _group.Minutes.ToString()) : _group.Minutes.ToString());
            btnOK.Visibility = Visibility.Hidden;
            btnCancel.Content = "ОК";
       }

        // инициализация combobox, в котором выбирается время через которое надо удалять копии
        private ComboBoxItem InitComboItem(int num)
        {
            ComboBoxItem res = new ComboBoxItem();
            if (num == 0)
            {
                res.Content = "Не удалять";
                return res;
            }

            string days;
            if (num > 10 && num < 15) { days = "дней"; }
            else if (num % 10 == 1) { days = "день"; }
            else if (num % 10 < 5 && num % 10 > 0) { days = "дня"; }
            else { days = "дней"; }
            res.Content = num.ToString() + " " + days;
            return res;
        }

        
        //отмена
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //распарсить время с формочки и вернуть в _h, _m
        private bool ParseTime(ref int _h, ref int _m)
        {
            bool result = false;
            string time = boxTime.Text.Trim();
            Match m = Regex.Match(time, @"^(\d{1,2}):(\d{1,2})$");
            if (m.Success)
            {
                int h = int.Parse(m.Groups[1].Value);
                int min = int.Parse(m.Groups[2].Value);
                if (h <= 24)
                {
                    if (h == 24) { h = 0; }
                    if (min < 60)
                    {
                        _h = h;
                        _m = min;
                        result = true;
                    }
                }       
            }
            return result;
        }

        //распарсить все с формочки и вернуть TaskGroup
        private ScheduleTaskGroup GetScheduleTaskGroup()
        {
            int h = 0; int m = 0;
            if (!ParseTime(ref h, ref m))
                throw new ScheduleException("Время необходимо ввести в формате HH:MM");
            BackupType t = BackupType.Full;
            //if ((bool)rbtnFull.IsChecked) { t = BackupType.Full ;  }
            if ((bool)rbtnIncr.IsChecked) { t = BackupType.Incremental; }
            if ((bool)rbtnDiff.IsChecked) { t = BackupType.Differential; }

            CheckBox[] cbtns = new CheckBox[] { cboxSun, cboxMon, cboxTue, cboxWen, cboxThu, cboxFri, cboxSat };
            List<DayOfWeek> days = new List<DayOfWeek>();
            for (int i = 0; i < cbtns.Length; i++)
            {
                if ((bool)cbtns[i].IsChecked)
                    days.Add((DayOfWeek)i);
            }
            if (days.Count == 0)
                throw new ScheduleException("Не задан ни один день недели");
            int repeat = cboxRepeatTime.SelectedIndex+1;
            int ttl = cboxTTL.SelectedIndex;
            return new ScheduleTaskGroup(t, days.ToArray(), repeat, ttl, h, m);
        }

        // создать расписание и закрыть формочку
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_schedule = GetScheduleTaskGroup();
                m_isOk = true;
                Close();
            }
            catch (ScheduleException ex)
            {
                ErrorBox.Show(ex.Message);
                gboxDays.Focus();
            }
        }
    }
}
