﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace BackupPC
{

    public static class Log
    {
        private static RichTextBox m_logBox;
        private delegate void AddStr(string _text);
        private static AddStr m_delegateAppendStr;
        private static object m_sync = new object();

        public static void Init(RichTextBox _logBox)
        {
            m_logBox = _logBox;
            m_delegateAppendStr = new AddStr(AppendString);
        }

        public static void Add(string _logString)
        {
            lock (m_sync)
            {
                if (m_logBox == null)
                    return;

                m_logBox.Dispatcher.Invoke(
                    m_delegateAppendStr,
                    new object[] { _logString });
            }
        }

        private static void AppendString(string _str)
        {
            m_logBox.AppendText(">" + _str + "\r\n");
            m_logBox.PageDown();
        }
    }
}
