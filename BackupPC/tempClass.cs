﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SQLite;
using System.Text.RegularExpressions;
using System.Threading;
namespace BackupPC
{

    public class FileExplorer
    {
        private static FileExplorer m_explorerInstance=null;

        private bool m_isEnd;
        private string m_sourcePath;
        private Stack<string> m_relativePaths;
        private Stack<List<DirectoryInfo>> m_directories;

        public static FileExplorer Explorer
        {
            get {
                if (m_explorerInstance == null) { m_explorerInstance = new FileExplorer(); }
                return m_explorerInstance;
            }
        }

        public bool IsEnd { get { return m_isEnd; } }

        private FileExplorer() 
        {
            m_isEnd = true;
            m_relativePaths = new Stack<string>();
            m_directories = new Stack<List<DirectoryInfo>>();
        }

        public void ChangeDirectory(string _path)
        {
            m_isEnd = false;
            m_directories.Clear();
            m_relativePaths.Clear();

            m_sourcePath = _path;
            m_relativePaths.Push("");
            
        }

        public List<FileInfo> GetNextFiles(out string _relPath)
        {
            FileInfo[] files = new FileInfo[]{};
            _relPath = "";
            string path = "";
            try
            {
                if (m_isEnd) throw new Exception("Nothing to observe!");
                _relPath = m_relativePaths.Peek();
                path = m_sourcePath + _relPath;
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                List<DirectoryInfo> dirs = dirInfo.GetDirectories().ToList();
                m_directories.Push(dirs);
                files = dirInfo.GetFiles();
                CD();
            }
            catch(Exception ex)
            {
                Debug.WriteLine(">>>FileExplorer: " + ex.Message);
                CD();
            }
            return files.ToList();
        }

        private void CD()
        {
            List<DirectoryInfo> listDirs = m_directories.Peek();
            if (listDirs.Count == 0)
            {
                if (m_directories.Count == 1)
                    m_isEnd = true;
                else 
                {
                    m_directories.Pop();
                    m_relativePaths.Pop();
                    CD();
                }
            }
            else
            {
                DirectoryInfo dir = listDirs[0];
                listDirs.RemoveAt(0);
                string newRelPath = m_relativePaths.Peek() + @"\" + dir.Name;
                m_relativePaths.Push(newRelPath);
            }
        }
    }

    public abstract class AbsTask
    {
        protected Backup m_backup;
        protected ScheduleTask m_task;
        protected CopyMethod m_copy;
        public abstract void Start(DBQuery _query);
        public abstract void Finish(DBQuery _query);
    }

    public class FullBackupTask : AbsTask
    {
        public FullBackupTask(ScheduleTask _task, CopyMethod _copy)
        {
            m_task = _task;
            m_backup = m_task.Parent.Backup;
            m_copy = _copy;
        }

        public override void Start(DBQuery _query)
        {
            FileExplorer explorer = FileExplorer.Explorer;
            BackupDir[] sourceDirs = m_backup.SourceDirs;

            _query.ClearTempTable();
            m_copy.SetTargetPath(m_backup.TargetDir.Value);
            foreach (BackupDir dir in sourceDirs)
            {
                explorer.ChangeDirectory(dir.Value);
                string targetDir = m_backup.TargetDir.Value;
                while (!explorer.IsEnd)
                {
                    string relPath;
                    try
                    {
                    List<FileInfo> files = explorer.GetNextFiles(out relPath);
                    m_copy.Copy(files, relPath, dir.ID, _query);
                    _query.AddFilesToTmp(files, dir.ID, relPath);
                    }
                    catch
                    {
                        throw new Exception();
                    }
                }
            }
        }

        public override void Finish(DBQuery _query)
        {
            _query.ClearFileFolderTables(m_backup);
            _query.UpdateAllFoldersTmp();
            _query.UpdateFilesFromTmp();
            _query.InsertFilesFromTmp();
        }

    }

    public class OtherBackupTask : AbsTask
    {
        public OtherBackupTask(ScheduleTask _task, CopyMethod _copy)
        {
            m_task = _task;
            m_backup = m_task.Parent.Backup;
            m_copy = _copy;
        }

        public override void Start(DBQuery _query)
        {
            FileExplorer explorer = FileExplorer.Explorer;
            BackupDir[] sourceDirs = m_backup.SourceDirs;

            _query.ClearTempTable();
            m_copy.SetTargetPath(m_backup.TargetDir.Value);
            foreach (BackupDir dir in sourceDirs)
            {
                explorer.ChangeDirectory(dir.Value);
                string targetDir = m_backup.TargetDir.Value;
                while (!explorer.IsEnd)
                {
                    string relPath;
                    
                        List<FileInfo> files = explorer.GetNextFiles(out relPath);
                        _query.AddNewFilesToTmp(ref files, dir.ID, relPath);
                        m_copy.Copy(files, relPath, dir.ID, _query);
                }
            }
        }

        public override void Finish(DBQuery _query)
        {
            if (m_task.Parent.Type == BackupType.Incremental)
            {
                _query.UpdateAllFoldersTmp();
                _query.UpdateFilesFromTmp();
                _query.InsertFilesFromTmp();
            }
        }
    }
 

    public abstract class CopyMethod
    {
        public string m_container;
        protected string m_prefix = "";
        public long CommonSize = 0;
        public int FileCount = 0;
        public abstract void Copy(List<FileInfo> _files, string _relPath, long _sourceID, DBQuery _query);
        public abstract void SetTargetPath(string _targetPath);
        public void SetPrefix(string _prefix)
        {
            m_prefix = _prefix;
            m_prefix = m_prefix.Replace(":", "_");
        }
    }

    public class ArchiveCopyMethod:CopyMethod
    {
        public override void Copy(List<FileInfo> _files, string _relPath, long _sourceID, DBQuery _query)
        {
            string path = m_container + _relPath;
            foreach (FileInfo file in _files)
            {
                file.CopyTo(path + file.Name);
            }
        }

        public override void SetTargetPath(string _targetPath)
        {
            m_container = _targetPath + @"\" + m_prefix;
        }
    }

    public class FolderCopyMethod : CopyMethod
    {

        public override void Copy(List<FileInfo> _files, string _relPath, long _sourceID, DBQuery _query)
        {
            string path = m_container + @"\" + _sourceID.ToString() + _relPath;
            if (_files.Count > 0)
                Directory.CreateDirectory(path);
            foreach (FileInfo file in _files)
            {
                string filepath = path + @"\" + file.Name;
                try
                {
                    file.CopyTo(filepath);
                    FileCount++;
                    CommonSize += file.Length;
                }
                catch
                {
                    Log.Add("Невозможно получить доступ к файлу: " + file.FullName);
                    _query.DeleteFileFromTmp(file.Name, _relPath, _sourceID);
                }
            }
        }

        public override void SetTargetPath(string _targetPath)
        {
            m_container = _targetPath + @"\" + m_prefix;
        }
    }

}
        